import os
import pytest
from hash_validator import hashalg


class HashInfo:
    def __init__(self, file_path, algorithm, checksum):
        self.file_path = file_path
        self.algorithm = algorithm
        self.checksum = checksum


def hashes_info():
    package_path = os.path.abspath(__package__)
    file_path = os.path.join(package_path, "static", "Badimallik.png")
    return [
        HashInfo(file_path, "md5", "39e1a29590a4a2a342d9e61c31f7b39f"),
        HashInfo(file_path, "sha1", "b8c82d53dee6795164a1145d9332c11ac8904a6a"),
        HashInfo(file_path, "sha224", "0d739682580074bfe38c27ed96d6b5973eb051644eccf942a420026d"),
        HashInfo(file_path, "sha256", "48bb050e41ee6e649814046f27b5d42adc932a946af389b91471859f9807b6e8"),
        HashInfo(file_path, "sha384", "5b82ccce4bc7951e161b46b1a47aabd273b42a47b619a59fb2021e0214c6885e0898a264da85ece233b45d7655d1cff3"),
        HashInfo(file_path, "sha512", "3216a8794ff0b654117728e32cc251ef1e9f8e004cd6592fb3c72ec0e27fc06a6ab1c0edcd71e23746d6f7e84d5cd60276db288db5e4037aae9077915b176f64")
    ]


@pytest.mark.parametrize("hash_info", hashes_info())
def test_calc_checksum(hash_info):
    algorithm = hashalg.get_algorithm(hash_info.algorithm)
    result = algorithm.calc_checksum(hash_info.file_path)
    assert result == hash_info.checksum


@pytest.mark.parametrize("hash_info", hashes_info())
def test_check_hash(hash_info):
    assert hashalg.check_hash(hash_info.file_path, hash_info.checksum) is True


@pytest.mark.parametrize("hash_info", hashes_info())
def test_determine_algorithm_name(hash_info):
    result = hashalg.determine_algorithm_name(hash_info.checksum)
    assert result == hash_info.algorithm


def test_unrecognized_algorithm():
    assert hashalg.get_algorithm("") is None


def test_invalid_file():
    invalid_file_path = "/invalid_file_path/invalid.path"
    algorithm = hashalg.get_algorithm("md5")
    with pytest.raises(FileNotFoundError):
        algorithm.calc_checksum(invalid_file_path)
