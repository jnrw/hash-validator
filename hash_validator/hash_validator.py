#!/usr/bin/env python3
import os
import sys

import hashalg
import argparser


def display_invalid_inputs(parser):
    if not parser.validate_file_path():
        print("File is not valid.")
    if not parser.validate_checksum():
        print("Unrecognized checksum.")


def display_result_success():
        print("\u2714 Ok.")


def display_result_fail():
        print("\u2718 Not Ok.")


def main():
    parser = argparser.ArgParser()
    if not parser.validate_inputs():
        display_invalid_inputs(parser)
        exit(1)
    if hashalg.check_hash(parser.file_path, parser.checksum):
        display_result_success()
    else:
        display_result_fail()


if __name__ == "__main__":
    main()
