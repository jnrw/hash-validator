import os
import argparse

import hashalg


PROG_NAME = "hash-validator"
USAGE = "hash-validator file checksum"
DESCRIPTION = "hash-validator is a script to validate checksums of downloaded files."


def _create_argparse():
    parser = argparse.ArgumentParser(PROG_NAME, USAGE, DESCRIPTION)
    parser.add_argument("file", help="File path")
    parser.add_argument("checksum", help="Checksum to be compared")
    return parser.parse_args()


class ArgParser:
    def __init__(self):
        self.parser = _create_argparse()
        self.file_path = self.parser.file
        self.checksum = self.parser.checksum
        
    def validate_file_path(self):
        return os.path.isfile(self.parser.file)
    
    def validate_checksum(self):
        return bool(hashalg.determine_algorithm_name(self.checksum))
    
    def validate_inputs(self):
        return all([self.validate_file_path(), self.validate_checksum()])
