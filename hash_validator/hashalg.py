import re
import hashlib


class Algorithm:
    def __init__(self, name, regex):
        self.name = name
        self.regex = regex

    def match(self, checksum):
        return bool(self.regex.match(checksum))

    def calc_checksum(self, file_path):
        hashing = hashlib.new(self.name)
        with open(file_path, "rb") as file:
            hashing.update(file.read())
        return hashing.hexdigest()


ALGORITHMS = {
    "md5": Algorithm("md5", re.compile("^[A-Fa-f0-9]{32}$")),
    "sha1": Algorithm("sha1", re.compile("^[A-Fa-f0-9]{40}$")),
    "sha224": Algorithm("sha224", re.compile("^[A-Fa-f0-9]{56}$")),
    "sha256": Algorithm("sha256", re.compile("^[A-Fa-f0-9]{64}$")),
    "sha384": Algorithm("sha384", re.compile("^[A-Fa-f0-9]{96}$")),
    "sha512": Algorithm("sha512", re.compile("^[A-Fa-f0-9]{128}$")),
}


def get_algorithm(name):
    return ALGORITHMS.get(name, None)


def determine_algorithm_name(checksum):
    found_algorithm = None
    for algorithm in ALGORITHMS.values():
        if algorithm.match(checksum):
            found_algorithm = algorithm
            break
    return found_algorithm.name if found_algorithm else None


def check_hash(file_path, checksum):
    algorithm_name = determine_algorithm_name(checksum)
    algorithm = get_algorithm(algorithm_name)
    calculated_checksum = algorithm.calc_checksum(file_path)
    return calculated_checksum == checksum
