# Hash Validator

hash-validator is a script to validate checksums of downloaded files.

# Usage

```sh
python3 hash-validator.py *file* *checksum*
```
